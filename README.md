# scprime-coldstorage

scprime-coldstorage is a utility that generates a seed and a collection of 
addresses for a ScPrime wallet, without the blockchain. This is useful for 
creating 'cold wallets'; you can run this utility on an airgapped machine, 
write down the seed and the addresses, and send coins safely to any of the 
addresses without having to worry about malware stealing your coins. You can 
then import the seed to a regular wallet to restore the seed and have access
to the funds.

## USAGE

Download the version for your computer, then double click the `scprime-
coldstorage` binary. This will generate a seed and several addresses in a 
web browswer window


## LICENSE

The MIT License (MIT)
